﻿using System.Collections.Generic;
using TIBS.stem.Auditing.Dto;
using TIBS.stem.Dto;

namespace TIBS.stem.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);
    }
}
