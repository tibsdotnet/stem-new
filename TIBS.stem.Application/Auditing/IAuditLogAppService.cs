using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TIBS.stem.Auditing.Dto;
using TIBS.stem.Dto;

namespace TIBS.stem.Auditing
{
    public interface IAuditLogAppService : IApplicationService
    {
        Task<PagedResultOutput<AuditLogListDto>> GetAuditLogs(GetAuditLogsInput input);

        Task<FileDto> GetAuditLogsToExcel(GetAuditLogsInput input);
    }
}