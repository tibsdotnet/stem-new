using System.Collections.Generic;
using TIBS.stem.Authorization.Users.Dto;
using TIBS.stem.Dto;

namespace TIBS.stem.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
    }
}