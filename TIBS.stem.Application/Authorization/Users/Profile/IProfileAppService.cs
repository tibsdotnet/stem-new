﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TIBS.stem.Authorization.Users.Profile.Dto;

namespace TIBS.stem.Authorization.Users.Profile
{
    public interface IProfileAppService : IApplicationService
    {
        Task<CurrentUserProfileEditDto> GetCurrentUserProfileForEdit();

        Task UpdateCurrentUserProfile(CurrentUserProfileEditDto input);
        
        Task ChangePassword(ChangePasswordInput input);
    }
}
