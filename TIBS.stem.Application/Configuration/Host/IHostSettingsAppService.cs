﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TIBS.stem.Configuration.Host.Dto;

namespace TIBS.stem.Configuration.Host
{
    public interface IHostSettingsAppService : IApplicationService
    {
        Task<HostSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(HostSettingsEditDto input);
    }
}
