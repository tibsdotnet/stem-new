﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TIBS.stem.Configuration.Tenants.Dto;

namespace TIBS.stem.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);
    }
}
