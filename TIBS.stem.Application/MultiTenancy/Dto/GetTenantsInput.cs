﻿using Abp.Runtime.Validation;
using TIBS.stem.Dto;

namespace TIBS.stem.MultiTenancy.Dto
{
    public class GetTenantsInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "TenancyName";
            }
        }
    }
}