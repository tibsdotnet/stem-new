﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TIBS.stem.Sessions.Dto;

namespace TIBS.stem.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
