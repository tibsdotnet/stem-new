﻿using Abp.Application.Services;
using TIBS.stem.Tenants.Dashboard.Dto;

namespace TIBS.stem.Tenants.Dashboard
{
    public interface ITenantDashboardAppService : IApplicationService
    {
        GetMemberActivityOutput GetMemberActivity();
    }
}
