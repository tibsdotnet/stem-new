﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TIBS.stem.Tenants.Phonebook.Dto;

namespace TIBS.stem.Tenants.Phonebook
{
    public interface IPersonAppService : IApplicationService
    {
        ListResultOutput<PersonListDto> GetPeople(GetPeopleInput input);
    }
}
