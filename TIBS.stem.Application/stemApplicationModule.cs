﻿using System.Reflection;
using Abp.AutoMapper;
using Abp.Modules;
using TIBS.stem.Authorization;

namespace TIBS.stem
{
    /// <summary>
    /// Application layer module of the application.
    /// </summary>
    [DependsOn(typeof(stemCoreModule), typeof(AbpAutoMapperModule))]
    public class stemApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Adding authorization providers
            Configuration.Authorization.Providers.Add<AppAuthorizationProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            //Custom DTO auto-mappings
            CustomDtoMapper.CreateMappings();
        }
    }
}
