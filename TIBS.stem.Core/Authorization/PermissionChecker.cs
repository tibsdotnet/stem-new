﻿using Abp.Authorization;
using TIBS.stem.Authorization.Roles;
using TIBS.stem.Authorization.Users;
using TIBS.stem.MultiTenancy;

namespace TIBS.stem.Authorization
{
    /// <summary>
    /// Implements <see cref="PermissionChecker"/>.
    /// </summary>
    public class PermissionChecker : PermissionChecker<Tenant, Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
