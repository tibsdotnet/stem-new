﻿using System.Collections.Generic;
using Abp.Configuration;

namespace TIBS.stem.Configuration
{
    /// <summary>
    /// Defines settings for the application.
    /// See <see cref="AppSettings"/> for setting names.
    /// </summary>
    public class AppSettingProvider : SettingProvider
    {
        public override IEnumerable<SettingDefinition> GetSettingDefinitions(SettingDefinitionProviderContext context)
        {
            return new[]
                   {
                       //Host settings
                       new SettingDefinition(AppSettings.General.WebSiteRootAddress, "http://localhost:6234/"),

                       //Tenant settings
                       new SettingDefinition(AppSettings.UserManagement.AllowSelfRegistration, "true", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.UserManagement.IsNewRegisteredUserActiveByDefault, "false", scopes: SettingScopes.Tenant),
                       new SettingDefinition(AppSettings.UserManagement.UseCaptchaOnRegistration, "true", scopes: SettingScopes.Tenant),
                   };
        }
    }
}
