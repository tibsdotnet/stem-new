﻿namespace TIBS.stem.Emailing
{
    public interface IEmailTemplateProvider
    {
        string GetDefaultTemplate();
    }
}
