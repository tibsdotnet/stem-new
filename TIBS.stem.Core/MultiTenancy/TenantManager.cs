﻿using Abp.Domain.Repositories;
using Abp.MultiTenancy;
using TIBS.stem.Authorization.Roles;
using TIBS.stem.Authorization.Users;

namespace TIBS.stem.MultiTenancy
{
    /// <summary>
    /// 
    /// </summary>
    public class TenantManager : AbpTenantManager<Tenant, Role, User>
    {
        public TenantManager(IRepository<Tenant> tenantRepository)
            : base(tenantRepository)
        {

        }
    }
}
