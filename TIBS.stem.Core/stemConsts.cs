﻿namespace TIBS.stem
{
    /// <summary>
    /// Some general constants for the application.
    /// </summary>
    public class stemConsts
    {
        public const string LocalizationSourceName = "stem";
    }
}