﻿using System.Data.Common;
using System.Data.Entity;
using Abp.Zero.EntityFramework;
using TIBS.stem.Authorization.Roles;
using TIBS.stem.Authorization.Users;
using TIBS.stem.MultiTenancy;
using TIBS.stem.Storage;

namespace TIBS.stem.EntityFramework
{
    public class stemDbContext : AbpZeroDbContext<Tenant, Role, User>
    {
        /* Define an IDbSet for each entity of the application */

        public virtual IDbSet<BinaryObject> BinaryObjects { get; set; }
        public virtual IDbSet<Person> Persons { get; set; }

        /* Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         * But it may cause problems when working Migrate.exe of EF. ABP works either way.         * 
         */
        public stemDbContext()
            : base("Default")
        {

        }

        /* This constructor is used by ABP to pass connection string defined in stemDataModule.PreInitialize.
         * Notice that, actually you will not directly create an instance of stemDbContext since ABP automatically handles it.
         */
        public stemDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        /* This constructor is used in tests to pass a fake/mock connection.
         */
        public stemDbContext(DbConnection dbConnection)
            : base(dbConnection, true)
        {

        }
    }
}
