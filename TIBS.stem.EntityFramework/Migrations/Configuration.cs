using System.Data.Entity.Migrations;
using TIBS.stem.Migrations.Seed;

namespace TIBS.stem.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<stem.EntityFramework.stemDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "stem";
        }

        protected override void Seed(stem.EntityFramework.stemDbContext context)
        {
            new InitialDbBuilder(context).Create();
        }
    }
}
