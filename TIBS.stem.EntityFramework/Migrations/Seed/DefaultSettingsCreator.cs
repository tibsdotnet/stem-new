﻿using System.Linq;
using Abp.Configuration;
using Abp.Net.Mail;
using TIBS.stem.EntityFramework;

namespace TIBS.stem.Migrations.Seed
{
    public class DefaultSettingsCreator
    {
        private readonly stemDbContext _context;

        public DefaultSettingsCreator(stemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            AddSettingIfNotExists(EmailSettingNames.DefaultFromAddress, "admin@mydomain.com");
            AddSettingIfNotExists(EmailSettingNames.DefaultFromDisplayName, "mydomain.com mailer");
        }

        private void AddSettingIfNotExists(string name, string value)
        {
            if (_context.Settings.Any(s => s.Name == name && s.TenantId == null && s.UserId == null))
            {
                return;
            }

            _context.Settings.Add(new Setting(null, null, name, value));
            _context.SaveChanges();
        }
    }
}