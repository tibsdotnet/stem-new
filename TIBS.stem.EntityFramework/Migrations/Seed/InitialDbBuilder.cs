﻿using EntityFramework.DynamicFilters;
using TIBS.stem.EntityFramework;

namespace TIBS.stem.Migrations.Seed
{
    public class InitialDbBuilder
    {
        private readonly stemDbContext _context;

        public InitialDbBuilder(stemDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            _context.DisableAllFilters();
            new DefaultTenantRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();
        }
    }
}
