﻿(function () {
    appModule.controller('common.views.layout.footer', [
        '$rootScope',
        function ($scope) {
            $scope.$on('$includeContentLoaded', function () {
                Layout.initFooter(); // init footer
            });
        }
    ]);
})();