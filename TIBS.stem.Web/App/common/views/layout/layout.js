﻿(function () {
    appModule.controller('common.views.layout', [
        '$scope',
        function ($scope) {
            $scope.$on('$viewContentLoaded', function () {
                Metronic.initComponents(); // init core components
            });
        }
    ]);
})();