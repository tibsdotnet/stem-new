﻿(function () {
    appModule.controller('host.views.tenants.createModal', [
        '$scope', '$modalInstance', 'abp.services.app.tenant',
        function ($scope, $modalInstance, tenantService) {
            var vm = this;

            vm.saving = false;
            vm.setRandomPassword = true;
            vm.tenant = {
                isActive: true,
                shouldChangePasswordOnNextLogin: true,
                sendActivationEmail: true
            };

            vm.save = function() {
                if (vm.setRandomPassword) {
                    vm.tenant.adminPassword = null;
                }

                vm.saving = true;
                tenantService.createTenant(vm.tenant)
                    .success(function() {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        $modalInstance.close();
                    }).finally(function() {
                        vm.saving = false;
                    });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };
        }
    ]);
})();