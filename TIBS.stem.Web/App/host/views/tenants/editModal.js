﻿(function () {
    appModule.controller('host.views.tenants.editModal', [
        '$scope', '$modalInstance', 'abp.services.app.tenant', 'tenantId',
        function ($scope, $modalInstance, tenantService, tenantId) {
            var vm = this;

            vm.saving = false;
            vm.tenant = null;

            vm.save = function () {
                vm.saving = true;
                tenantService.updateTenant(vm.tenant)
                    .success(function () {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        $modalInstance.close();
                    }).finally(function () {
                        vm.saving = false;
                    });
            };

            vm.cancel = function () {
                $modalInstance.dismiss();
            };

            function init() {
                tenantService.getTenantForEdit({
                    id: tenantId
                }).success(function (result) {
                    vm.tenant = result;
                });
            }

            init();
        }
    ]);
})();