﻿using Abp.Dependency;

namespace TIBS.stem.Web
{
    public class AppFolders : IAppFolders, ISingletonDependency
    {
        public string TempFileDownloadFolder { get; set; }
    }
}