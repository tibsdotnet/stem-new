﻿using System.Web.Optimization;

namespace TIBS.stem.Web.Bundling
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();
            RegisterFrontendBundles(bundles);
            RegisterAppBundles(bundles);
            RegisterCommonBundles(bundles);
        }

        private static void RegisterCommonBundles(BundleCollection bundles)
        {
            //COMMON BUNDLES USED BOTH IN FRONTEND AND BACKEND

            bundles.Add(
                new StyleBundle("~/Bundles/Common/css")
                    .IncludeDirectory("~/Common/Styles", "*.css", true)
                    .ForceOrdered()
                );

            bundles.Add(
                new ScriptBundle("~/Bundles/Common/js")
                    .IncludeDirectory("~/Common/Scripts", "*.js", true)
                    .ForceOrdered()
                );
        }


        public static void RegisterFrontendBundles(BundleCollection bundles)
        {
            //LIBRARIES

            bundles.Add(
                new StyleBundle("~/Bundles/Frontend/libs/css")
                    .Include(StylePaths.Simple_Line_Icons, new CssRewriteUrlTransform())
                    .Include(StylePaths.FontAwesome, new CssRewriteUrlTransform())
                    .Include(StylePaths.FamFamFamFlags, new CssRewriteUrlTransform())
                    .Include(StylePaths.Bootstrap, new CssRewriteUrlTransform())
                    .Include(StylePaths.SweetAlert)
                    .Include(StylePaths.Toastr)
                    .ForceOrdered()
                );

            bundles.Add(
                new ScriptBundle("~/Bundles/Frontend/libs/js")
                    .Include(
                        ScriptPaths.Json2,
                        ScriptPaths.JQuery,
                        ScriptPaths.JQuery_Migrate,
                        ScriptPaths.Bootstrap,
                        ScriptPaths.Bootstrap_Hover_Dropdown,
                        ScriptPaths.JQuery_Slimscroll,
                        ScriptPaths.JQuery_BlockUi,
                        ScriptPaths.JQuery_Cookie,
                        ScriptPaths.SpinJs,
                        ScriptPaths.SpinJs_JQuery,
                        ScriptPaths.SweetAlert,
                        ScriptPaths.Toastr,
                        ScriptPaths.MomentJs,
                        ScriptPaths.Abp,
                        ScriptPaths.Abp_JQuery,
                        ScriptPaths.Abp_Toastr,
                        ScriptPaths.Abp_BlockUi,
                        ScriptPaths.Abp_SpinJs,
                        ScriptPaths.Abp_SweetAlert
                    ).ForceOrdered()
                );

            //METRONIC

            bundles.Add(
                new StyleBundle("~/Bundles/Frontend/metronic/css")
                    .Include("~/metronic/assets/global/css/components.css", new CssRewriteUrlTransform())
                    .Include("~/metronic/assets/frontend/layout/css/style.css", new CssRewriteUrlTransform())
                    .Include("~/metronic/assets/frontend/pages/css/style-revolution-slider.css", new CssRewriteUrlTransform())
                    .Include("~/metronic/assets/frontend/layout/css/style-responsive.css", new CssRewriteUrlTransform())
                    .Include("~/metronic/assets/frontend/layout/css/themes/red.css", new CssRewriteUrlTransform())
                    .ForceOrdered()
                );

            bundles.Add(
                new ScriptBundle("~/Bundles/Frontend/metronic/js")
                    .Include(
                        "~/metronic/assets/frontend/layout/scripts/back-to-top.js",
                        "~/metronic/assets/frontend/layout/scripts/layout.js"
                    ).ForceOrdered()
                );
        }

        public static void RegisterAppBundles(BundleCollection bundles)
        {
            //LIBRARIES

            bundles.Add(
                new StyleBundle("~/Bundles/App/libs/css")
                    .Include(StylePaths.FontAwesome, new CssRewriteUrlTransform())
                    .Include(StylePaths.Simple_Line_Icons, new CssRewriteUrlTransform())
                    .Include(StylePaths.FamFamFamFlags, new CssRewriteUrlTransform())
                    .Include(StylePaths.Bootstrap, new CssRewriteUrlTransform())
                    .Include(StylePaths.JQuery_Uniform, new CssRewriteUrlTransform())
                    .Include(StylePaths.Morris)
                    .Include(StylePaths.JsTree, new CssRewriteUrlTransform())
                    .Include(StylePaths.SweetAlert)
                    .Include(StylePaths.Toastr)
                    .Include(StylePaths.Angular_Ui_Grid, new CssRewriteUrlTransform())
                    .Include(StylePaths.Bootstrap_DateRangePicker)
                    .ForceOrdered()
                );

            bundles.Add(
                new ScriptBundle("~/Bundles/App/libs/js")
                    .Include(
                        ScriptPaths.Json2,
                        ScriptPaths.JQuery,
                        ScriptPaths.JQuery_Migrate,
                        ScriptPaths.Bootstrap,
                        ScriptPaths.Bootstrap_Hover_Dropdown,
                        ScriptPaths.JQuery_Slimscroll,
                        ScriptPaths.JQuery_BlockUi,
                        ScriptPaths.JQuery_Cookie,
                        ScriptPaths.JQuery_Uniform,
                        ScriptPaths.Morris,
                        ScriptPaths.Morris_Raphael,
                        ScriptPaths.JQuery_Sparkline,
                        ScriptPaths.JsTree,
                        ScriptPaths.SpinJs,
                        ScriptPaths.SpinJs_JQuery,
                        ScriptPaths.SweetAlert,
                        ScriptPaths.Toastr,
                        ScriptPaths.MomentJs,
                        ScriptPaths.Bootstrap_DateRangePicker,
                        ScriptPaths.Underscore,
                        ScriptPaths.Angular,
                        ScriptPaths.Angular_Sanitize,
                        ScriptPaths.Angular_Touch,
                        ScriptPaths.Angular_Ui_Router,
                        ScriptPaths.Angular_Ui_Utils,
                        ScriptPaths.Angular_Ui_Bootstrap_Tpls,
                        ScriptPaths.Angular_Ui_Grid,
                        ScriptPaths.Angular_OcLazyLoad,
                        ScriptPaths.Angular_File_Upload,
                        ScriptPaths.Angular_DateRangePicker,
                        ScriptPaths.Abp,
                        ScriptPaths.Abp_JQuery,
                        ScriptPaths.Abp_Toastr,
                        ScriptPaths.Abp_BlockUi,
                        ScriptPaths.Abp_SpinJs,
                        ScriptPaths.Abp_SweetAlert,
                        ScriptPaths.Abp_Angular
                    ).ForceOrdered()
                );

            //METRONIC

            bundles.Add(
                new StyleBundle("~/Bundles/App/metronic/css")
                    .Include("~/metronic/assets/global/css/components-md.css", new CssRewriteUrlTransform())
                    .Include("~/metronic/assets/global/css/plugins-md.css", new CssRewriteUrlTransform())
                    .Include("~/metronic/assets/admin/layout4/css/layout.css", new CssRewriteUrlTransform())
                    .Include("~/metronic/assets/admin/layout4/css/themes/light.css", new CssRewriteUrlTransform())
                    .ForceOrdered()
                );

            bundles.Add(
              new ScriptBundle("~/Bundles/App/metronic/js")
                  .Include(
                      "~/metronic/assets/global/scripts/metronic.js",
                      "~/metronic/assets/admin/layout4/scripts/layout.js"
                  ).ForceOrdered()
              );

            //APPLICATION

            bundles.Add(
                new StyleBundle("~/Bundles/App/css")
                    .IncludeDirectory("~/App", "*.css", true)
                    .ForceOrdered()
                );

            bundles.Add(
                new ScriptBundle("~/Bundles/App/js")
                    .IncludeDirectory("~/App", "*.js", true)
                    .ForceOrdered()
                );
        }
    }
}