﻿using System.Threading;
using Abp.Extensions;

namespace TIBS.stem.Web.Bundling
{
    public static class ScriptPaths
    {
        public const string Json2 = "~/libs/json2/json2.min.js";
        
        public const string JQuery = "~/libs/jquery/jquery.min.js";
        public const string JQuery_Migrate = "~/libs/jquery/jquery-migrate.min.js";
        
        public const string JQuery_Slimscroll = "~/libs/jquery-slimscroll/jquery.slimscroll.min.js";
        public const string JQuery_BlockUi = "~/libs/jquery-blockui/jquery.blockui.min.js";
        public const string JQuery_Cookie = "~/libs/jquery-cookie/jquery.cookie.min.js";
        public const string JQuery_Uniform = "~/libs/jquery-uniform/jquery.uniform.min.js";
        public const string JQuery_Sparkline = "~/libs/jquery-sparkline/jquery.sparkline.min.js";
        public const string JQuery_Validation = "~/libs/jquery-validation/js/jquery.validate.min.js";

        public const string Bootstrap = "~/libs/bootstrap/js/bootstrap.min.js";
        public const string Bootstrap_Hover_Dropdown = "~/libs/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js";
        public const string Bootstrap_DateRangePicker = "~/libs/bootstrap-daterangepicker/daterangepicker.js";

        public const string Morris = "~/libs/morris/morris.min.js";
        public const string Morris_Raphael = "~/libs/morris/raphael-min.js";

        public const string JsTree = "~/libs/jstree/jstree.js";
        public const string SpinJs = "~/libs/spinjs/spin.js";
        public const string SpinJs_JQuery = "~/libs/spinjs/jquery.spin.js";

        public const string SweetAlert = "~/libs/sweetalert/sweet-alert.min.js";
        public const string Toastr = "~/libs/toastr/toastr.min.js";

        public const string MomentJs = "~/Scripts/moment-with-locales.min.js";
        public const string Underscore = "~/Scripts/underscore.min.js";

        public const string Angular = "~/Scripts/angular.min.js";
        public const string Angular_Sanitize = "~/Scripts/angular-sanitize.min.js";
        public const string Angular_Touch = "~/Scripts/angular-touch.min.js";
        public const string Angular_Ui_Router = "~/Scripts/angular-ui-router.min.js";
        public const string Angular_Ui_Utils = "~/Scripts/angular-ui/ui-utils.min.js";
        public const string Angular_Ui_Bootstrap_Tpls = "~/Scripts/angular-ui/ui-bootstrap-tpls.min.js";
        public const string Angular_Ui_Grid = "~/libs/angular-ui-grid/ui-grid.min.js";
        public const string Angular_OcLazyLoad = "~/libs/angular-ocLazyLoad/ocLazyLoad.min.js";
        public const string Angular_File_Upload = "~/libs/angular-file-upload/angular-file-upload.min.js";
        public const string Angular_DateRangePicker = "~/libs/angular-daterangepicker/angular-daterangepicker.min.js";

        public const string Abp = "~/Abp/Framework/scripts/abp.js";
        public const string Abp_JQuery = "~/Abp/Framework/scripts/libs/abp.jquery.js";
        public const string Abp_Toastr = "~/Abp/Framework/scripts/libs/abp.toastr.js";
        public const string Abp_BlockUi = "~/Abp/Framework/scripts/libs/abp.blockUI.js";
        public const string Abp_SpinJs = "~/Abp/Framework/scripts/libs/abp.spin.js";
        public const string Abp_SweetAlert = "~/Abp/Framework/scripts/libs/abp.sweet-alert.js";
        public const string Abp_Angular = "~/Abp/Framework/scripts/libs/angularjs/abp.ng.js";

        public static string JQuery_Validation_Localization
        {
            get
            {
                switch (Thread.CurrentThread.CurrentUICulture.Name.Left(2).ToLower())
                {
                    case "tr":
                        return "~/libs/jquery-validation/js/localization/messages_tr.min.js";
                    default:
                        return "~/libs/jquery-validation/js/localization/_messages_empty.js";
                }
            }
        }
    }
}