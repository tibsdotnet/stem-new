namespace TIBS.stem.Web.Bundling
{
    public static class StylePaths
    {
        public const string Simple_Line_Icons = "~/libs/simple-line-icons/simple-line-icons.min.css";
        public const string Bootstrap = "~/libs/bootstrap/css/bootstrap.min.css";
        public const string SweetAlert = "~/libs/sweetalert/sweet-alert.css";
        public const string Toastr = "~/libs/toastr/toastr.min.css";
        public const string FontAwesome = "~/Content/font-awesome.min.css";
        public const string FamFamFamFlags = "~/Content/flags/famfamfam-flags.css";
        public const string JQuery_Uniform = "~/libs/jquery-uniform/css/uniform.default.css";
        public const string Morris = "~/libs/morris/morris.css";
        public const string JsTree = "~/libs/jstree/themes/default/style.css";
        public const string Angular_Ui_Grid = "~/libs/angular-ui-grid/ui-grid.min.css";
        public const string Bootstrap_DateRangePicker = "~/libs/bootstrap-daterangepicker/daterangepicker-bs3.css";
    }
}