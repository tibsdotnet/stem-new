﻿using Abp.Application.Navigation;
using Abp.Localization;
using TIBS.stem.Authorization;

namespace TIBS.stem.Web.Navigation
{
    /// <summary>
    /// This class defines menus for the application.
    /// It uses ABP's menu system.
    /// When you add menu items here, they are automatically appear in angular application.
    /// See .cshtml and .js files under App/Main/views/layout/header to know how to render menu.
    /// </summary>
    public class AppNavigationProvider : NavigationProvider
    {
        public const string FrontendMenuName = "Frontend";

        public override void SetNavigation(INavigationProviderContext context)
        {
            SetAppNavigation(context);
            SetFrontendNavigation(context);
        }

        private void SetAppNavigation(INavigationProviderContext context)
        {
            context.Manager.MainMenu
                .AddItem(new MenuItemDefinition(
                    PageNames.App.Host.Tenants,
                    L("Tenants"),
                    url: "host.tenants",
                    icon: "glyphicon glyphicon-globe",
                    requiredPermissionName: AppPermissions.Pages_Tenants
                    )
                ).AddItem(new MenuItemDefinition(
                    PageNames.App.Tenant.Dashboard,
                    L("Dashboard"),
                    url: "tenant.dashboard",
                    icon: "glyphicon glyphicon-home",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Dashboard
                    )
                )
                .AddItem(new MenuItemDefinition(
                    PageNames.App.Common.Administration,
                    L("Administration"),
                    icon: "glyphicon glyphicon-wrench"
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Common.Roles,
                        L("Roles"),
                        url: "roles",
                        icon: "icon-briefcase",
                        requiredPermissionName: AppPermissions.Pages_Administration_Roles
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Common.Users,
                        L("Users"),
                        url: "users",
                        icon: "icon-users",
                        requiredPermissionName: AppPermissions.Pages_Administration_Users
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Common.AuditLogs,
                        L("AuditLogs"),
                        url: "auditLogs",
                        icon: "icon-lock",
                        requiredPermissionName: AppPermissions.Pages_Administration_AuditLogs
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Host.Settings,
                        L("Settings"),
                        url: "host.settings",
                        icon: "icon-settings",
                        requiredPermissionName: AppPermissions.Pages_Administration_Host_Settings
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.Settings,
                        L("Settings"),
                        url: "tenant.settings",
                        icon: "icon-settings",
                        requiredPermissionName: AppPermissions.Pages_Administration_Tenant_Settings
                        )
                    )
                ).AddItem(new MenuItemDefinition(
    PageNames.App.Common.Phonebook,
    L("Phonebook"),
    url: "tenant.phonebook",
    icon: "glyphicon glyphicon-book"
));
        }

        private void SetFrontendNavigation(INavigationProviderContext context)
        {
            var frontEndMenu = new MenuDefinition(FrontendMenuName, new FixedLocalizableString("Frontend menu"));
            context.Manager.Menus[FrontendMenuName] = frontEndMenu;

            frontEndMenu

                //HOME
                .AddItem(new MenuItemDefinition(
                    PageNames.Frontend.Home,
                    L("HomePage"),
                    url: ""
                    )

                //ABOUT
                ).AddItem(new MenuItemDefinition(
                    PageNames.Frontend.About,
                    L("AboutUs"),
                    url: "About"
                    )

                //MULTI-LEVEL MENU (JUST FOR EXAMPLE)
                //).AddItem(new MenuItemDefinition(
                //    "MultiLevelMenu",
                //    new FixedLocalizableString("Multi level menu")
                //    ).AddItem(new MenuItemDefinition(
                //        "MultiLevelMenu.1",
                //        new FixedLocalizableString("Sub menu item 1")
                //        )
                //    ).AddItem(new MenuItemDefinition(
                //        "MultiLevelMenu.2",
                //        new FixedLocalizableString("Sub menu item 2")
                //        ).AddItem(new MenuItemDefinition(
                //            "MultiLevelMenu.2.1",
                //            new FixedLocalizableString("Sub menu item 2.1")
                //            )
                //        ).AddItem(new MenuItemDefinition(
                //            "MultiLevelMenu.2.2",
                //            new FixedLocalizableString("Sub menu item 2.2")
                //            )
                //        ).AddItem(new MenuItemDefinition(
                //            "MultiLevelMenu.2.3",
                //            new FixedLocalizableString("Sub menu item 2.3")
                //            ).AddItem(new MenuItemDefinition(
                //                "MultiLevelMenu.2.3.1",
                //                new FixedLocalizableString("ASP.NET Boilerplate"),
                //                url: "http://aspnetboilerplate.com"
                //                )
                //            ).AddItem(new MenuItemDefinition(
                //                "MultiLevelMenu.2.3.2",
                //                new FixedLocalizableString("jtable.org"),
                //                url: "http://jtable.org"
                //                )
                //            )
                //        )
                //    )
                );
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, stemConsts.LocalizationSourceName);
        }
    }
}
