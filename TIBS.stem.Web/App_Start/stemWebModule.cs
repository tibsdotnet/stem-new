﻿using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Abp.IO;
using Abp.Localization;
using Abp.Modules;
using TIBS.stem.Web.Bundling;
using TIBS.stem.Web.Navigation;
using TIBS.stem.Web.Routing;
using TIBS.stem.WebApi;

namespace TIBS.stem.Web
{
    /// <summary>
    /// Web module of the application.
    /// This is the most top and entrance module that dependens on others.
    /// </summary>
    [DependsOn(
        typeof(stemDataModule), 
        typeof(stemApplicationModule), 
        typeof(stemWebApiModule))]
    public class stemWebModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Add/remove languages for your application
            Configuration.Localization.Languages.Add(new LanguageInfo("en", "English", "famfamfam-flag-gb", true));
            Configuration.Localization.Languages.Add(new LanguageInfo("tr", "Türkçe", "famfamfam-flag-tr"));
            Configuration.Localization.Languages.Add(new LanguageInfo("zh-CN", "简体中文", "famfamfam-flag-cn"));

            //Configure navigation/menu
            Configuration.Navigation.Providers.Add<AppNavigationProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        public override void PostInitialize()
        {
            var tempDownloadFolder = HttpContext.Current.Server.MapPath("~/Temp/Downloads");
            IocManager.Resolve<AppFolders>().TempFileDownloadFolder = tempDownloadFolder;

            try { DirectoryHelper.CreateIfNotExists(tempDownloadFolder); } catch { }
        }
    }
}
