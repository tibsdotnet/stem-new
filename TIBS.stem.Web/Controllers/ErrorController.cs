﻿using System.Web.Mvc;
using Abp.Auditing;

namespace TIBS.stem.Web.Controllers
{
    public class ErrorController : stemControllerBase
    {
        [DisableAuditing]
        public ActionResult E404()
        {
            return View();
        }
    }
}