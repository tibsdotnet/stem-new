using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.IO.Extensions;
using Abp.Runtime.Session;
using Abp.UI;
using TIBS.stem.Authorization.Users;
using TIBS.stem.Net.MimeTypes;
using TIBS.stem.Storage;

namespace TIBS.stem.Web.Controllers
{
    public class ProfileController : stemControllerBase
    {
        private readonly UserManager _userManager;
        private readonly IBinaryObjectManager _binaryObjectManager;

        public ProfileController(UserManager userManager, IBinaryObjectManager binaryObjectManager)
        {
            _userManager = userManager;
            _binaryObjectManager = binaryObjectManager;
        }

        [AbpAuthorize]
        [DisableAuditing]
        public async Task<FileResult> GetProfilePicture()
        {
            var user = await _userManager.GetUserByIdAsync(AbpSession.GetUserId());
            if (user.ProfilePictureId == null)
            {
                return GetDefaultProfilePicture();
            }

            return await GetProfilePictureById(user.ProfilePictureId.Value);
        }

        [AbpAuthorize]
        [DisableAuditing]
        public async Task<FileResult> GetProfilePictureById(string id = "")
        {
            if (id.IsNullOrEmpty())
            {
                return GetDefaultProfilePicture();                
            }

            return await GetProfilePictureById(Guid.Parse(id));
        }       

        [AbpAuthorize]
        [UnitOfWork]
        public async virtual Task ChangeProfilePicture()
        {
            if (Request.Files.Count <= 0 || Request.Files[0] == null)
            {
                throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
            }
            
            var file = Request.Files[0];

            if (file.ContentLength > 30720) //30KB.
            {
                throw new UserFriendlyException(L("ProfilePicture_Warn_SizeLimit"));
            }

            var user = await _userManager.GetUserByIdAsync(AbpSession.GetUserId());

            //Delete old picture
            if (user.ProfilePictureId.HasValue)
            {
                await _binaryObjectManager.DeleteAsync(user.ProfilePictureId.Value);
            }

            //Save new picture
            var storedFile = new BinaryObject(file.InputStream.GetAllBytes());
            await _binaryObjectManager.SaveAsync(storedFile);

            //Update new picture on the user
            user.ProfilePictureId = storedFile.Id;
        }

        private FileResult GetDefaultProfilePicture()
        {
            return File(Server.MapPath("~/App/common/images/default-profile-picture.png"), MimeTypeNames.ImagePng);
        }

        private async Task<FileResult> GetProfilePictureById(Guid profilePictureId)
        {
            var file = await _binaryObjectManager.GetOrNullAsync(profilePictureId);
            if (file == null)
            {
                return GetDefaultProfilePicture();
            }

            return File(file.Bytes, MimeTypeNames.ImageJpeg);
        }
    }
}