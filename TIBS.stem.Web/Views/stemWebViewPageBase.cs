﻿using Abp.Web.Mvc.Views;

namespace TIBS.stem.Web.Views
{
    public abstract class stemWebViewPageBase : stemWebViewPageBase<dynamic>
    {

    }

    public abstract class stemWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected stemWebViewPageBase()
        {
            LocalizationSourceName = stemConsts.LocalizationSourceName;
        }
    }
}