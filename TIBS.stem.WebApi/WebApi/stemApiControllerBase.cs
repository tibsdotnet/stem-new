using Abp.WebApi.Controllers;

namespace TIBS.stem.WebApi
{
    public abstract class stemApiControllerBase : AbpApiController
    {
        protected stemApiControllerBase()
        {
            LocalizationSourceName = stemConsts.LocalizationSourceName;
        }
    }
}